package com.sherinaruryanggreini_10191080.dashboardmbanking;

import java.util.ArrayList;

public class DatasData {
    private static String[] datasNames = {
            "Dribble",
            "Netflix",
            "Spotify",
            "Amazon",
    };
    private static String[] datasDetails = {
            "Rp. 120.000,-",
            "Rp. 1.800.000,-",
            "Rp. 85.000,-",
            "Rp. 515.500,-"
    };
    private static int[] datasImages = {
            R.drawable.dribble,
            R.drawable.netflix,
            R.drawable.spotify,
            R.drawable.amazon
    };
    static ArrayList<Data> getListData() {
        ArrayList<Data> list = new ArrayList<>();
        for (int position = 0; position < datasNames.length; position++) {
            Data data = new Data();
            data.setName(datasNames[position]);
            data.setDetail(datasDetails[position]);
            data.setPhoto(datasImages[position]);
            list.add(data);
        }
        return list;
    }
}
