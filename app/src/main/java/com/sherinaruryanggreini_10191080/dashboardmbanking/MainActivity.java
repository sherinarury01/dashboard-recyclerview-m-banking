package com.sherinaruryanggreini_10191080.dashboardmbanking;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvDatas;
    private ArrayList<Data> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvDatas = findViewById(R.id.rv_datas);
        rvDatas.setHasFixedSize(true);

        list.addAll(DatasData.getListData());
        showRecyclerList();
    }

    private void showRecyclerList() {
        rvDatas.setLayoutManager(new LinearLayoutManager(this));
        ListDataAdapter listHeroAdapter = new ListDataAdapter(list);
        rvDatas.setAdapter(listHeroAdapter);
    }
}